#make all o make gbison && make gflex && make

BISON = calc.tab.c
FBISON = calc.y
HBISON = calc.tab.h
FLEX = lex.yy.c
FFLEX = calc.l
HFLEX = flex.h
MAIN= main

CFLAGS= -std=gnu99
CC=gcc $(CFLAGS) -Wall

LIBSINCLUDE= -lm -ldl #-lfl -ll -lm
LIBS =
HEADER_FILES_DIR = ./
INCLUDES = -I $(HEADER_FILES_DIR)

SRCS = $(wildcard *.c)
DEPS = $(wildcard *.h)
OBJS = $(SRCS:.c=.o)

clean=rm -f $(MAIN) *.o $(FLEX) $(HFLEX) $(BISON) $(HBISON)
gflex=flex --header-file=$(HFLEX) $(FFLEX)
gbison=bison -v -d $(FBISON)
all=$(clean) && $(gbison) && $(gflex) && make

$(MAIN): $(OBJS)
	$(CC) -o $(MAIN) $(OBJS) $(LIBS) $(LIBSINCLUDE)
*.o: *.c $(DEPS)
	$(CC) -c $< $(INCLUDES)

clean:
	$(clean)
gbison:
	$(gbison)
gflex:
	$(gflex)
all:
	$(all)
