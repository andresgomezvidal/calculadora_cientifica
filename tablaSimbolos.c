#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tablaSimbolos.h"
#include "hash.h"
#include "error.h"


elementoTS **tablaSimbolos=NULL;												//TS: tabla hash de structs encadenados


//función auxiliar para simplificar el código
//devuelve el elemento a insertar en la TS
elementoTS* generarElementoTS(char *lexema, double valor, char tipo, double (*fnct)(), char * argumentos){
	elementoTS *elemento=NULL;
	//reserva memoria para el elemento de la tabla
	if((elemento=(elementoTS *)malloc(sizeof(elementoTS)))==NULL){										//si es NULL hubo un error
		error(4, lexema);
	}
	//reserva memoria para el string
	if((elemento->lexema=(char *)malloc(sizeof(char)*(1+strlen(lexema))))==NULL){						//si es NULL hubo un error
		error(5, lexema);
	}
	strcpy(elemento->lexema,lexema);				//no hace falta comprobar longitud porque ya se comprueba para el lexema recibido. Copia el string
	elemento->siguiente=NULL;						//se marca con NULL para saber que no está en uso
	elemento->tipo=tipo;

	if(tipo!=TIPO_FUN){ 							//si no es una funcion es una variable
		elemento->utilidad.valor=valor;
	}else{
		if((elemento->utilidad.fun=(sfuncion *)malloc(sizeof(sfuncion)))==NULL){								//NULL es error
			error(4, argumentos);
		}
		if((elemento->utilidad.fun->argumentos=(char *)malloc(sizeof(char)*(1+strlen(argumentos))))==NULL){	//NULL es error
			error(5, argumentos);
		}
		elemento->utilidad.fun->fnct=fnct;
		strcpy(elemento->utilidad.fun->argumentos,argumentos);
	}

	return elemento;
}


//inserta en la posición correcta en la TS si no existe el lexema
//devuelve el elemento insertado o el existente en la TS para ese lexema
elementoTS* insertarTS(char *lexema, double valor, char tipo, double (*fnct)(), char * argumentos){
	unsigned int posicion=hash(lexema,tamanoTS);									//calcula la posición

	if(tablaSimbolos[posicion]==NULL){												//posición libre: se inserta directamente
		tablaSimbolos[posicion]=generarElementoTS(lexema, valor, tipo, fnct, argumentos);
		return tablaSimbolos[posicion];
	}else{ 																			//posicion ocupada: se comprueba si no existe el lexema
		//compara el lexema para el primer elemento
		if((strcmp(lexema, tablaSimbolos[posicion]->lexema))==0){					//ya existe
			return tablaSimbolos[posicion];
		}
		if(tablaSimbolos[posicion]->siguiente!=NULL){								//busca el siguiente libre y mientras compara el lexema
			elementoTS *siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){								//comprueba si está libre
				if((strcmp(lexema,siguienteElemento->siguiente->lexema))==0){		//ya existe
					return siguienteElemento->siguiente;
				}
				siguienteElemento=siguienteElemento->siguiente;						//se pasa al siguiente elemento
			}
			siguienteElemento->siguiente=generarElementoTS(lexema, valor, tipo, fnct, argumentos);			//inserta en último libre
			return siguienteElemento->siguiente;
		}else{																		//el siguiente estaba libre: inserta
			tablaSimbolos[posicion]->siguiente=generarElementoTS(lexema, valor, tipo, fnct, argumentos);
			return tablaSimbolos[posicion]->siguiente;
		}
	}
}

//guarda la constante si su tipo es variable no inicializada
void guardarConstante(char *lexema, double valor){
	unsigned int posicion=hash(lexema,tamanoTS);									//calcula la posición

	if(tablaSimbolos[posicion]!=NULL){
		if((tablaSimbolos[posicion]->tipo==TIPO_VAR_U) && (strcmp(lexema, tablaSimbolos[posicion]->lexema))==0){
			tablaSimbolos[posicion]->tipo=TIPO_CON;
			tablaSimbolos[posicion]->utilidad.valor=valor;
			return;
		}
		if(tablaSimbolos[posicion]->siguiente!=NULL){								//busca la variable sin definir con ese lexema
			elementoTS *siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){
				if((tablaSimbolos[posicion]->tipo==TIPO_VAR_U) && (strcmp(lexema,siguienteElemento->siguiente->lexema))==0){
					siguienteElemento->siguiente->tipo=TIPO_CON;
					siguienteElemento->siguiente->utilidad.valor=valor;
					return;
				}
				siguienteElemento=siguienteElemento->siguiente;						//se pasa al siguiente elemento
			}
		}
	}
}

//guarda el valor en la variable si su tipo es variable o no inicializada
void guardarVariable(char *lexema, double valor){
	unsigned int posicion=hash(lexema,tamanoTS);									//calcula la posición

	if(tablaSimbolos[posicion]!=NULL){
		if((tablaSimbolos[posicion]->tipo==TIPO_VAR || tablaSimbolos[posicion]->tipo==TIPO_VAR_U) && (strcmp(lexema, tablaSimbolos[posicion]->lexema))==0){
			tablaSimbolos[posicion]->tipo=TIPO_VAR;
			tablaSimbolos[posicion]->utilidad.valor=valor;
			return;
		}
		if(tablaSimbolos[posicion]->siguiente!=NULL){								//busca la variable sin definir con ese lexema
			elementoTS *siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){
				if((tablaSimbolos[posicion]->tipo==TIPO_VAR || tablaSimbolos[posicion]->tipo==TIPO_VAR_U) && (strcmp(lexema,siguienteElemento->siguiente->lexema))==0){
					siguienteElemento->siguiente->tipo=TIPO_VAR;
					siguienteElemento->siguiente->utilidad.valor=valor;
					return;
				}
				siguienteElemento=siguienteElemento->siguiente;						//se pasa al siguiente elemento
			}
		}
	}
}


//función para simplificar la liberación
void liberarElemento(elementoTS* elemento){
	if(elemento->lexema!=NULL){
		free(elemento->lexema);
			elemento->lexema=NULL;
		if(elemento->tipo==TIPO_FUN){
			if(elemento->utilidad.fun!=NULL){
				if(elemento->utilidad.fun->argumentos!=NULL){
					free(elemento->utilidad.fun->argumentos);
						elemento->utilidad.fun->argumentos=NULL;
				}
				free(elemento->utilidad.fun);
					elemento->utilidad.fun=NULL;
			}
		}
	}
}

//libera de forma ordenada la TS
//siempre se compara con NULL para determinar si un puntero es usado
//adicionalmente (no hace falta en este caso) se ponen a NULL los párametros liberados
void liberarTS(){
	for(unsigned int i=0;i<tamanoTS;i++){
		if(tablaSimbolos[i]!=NULL){
			liberarElemento(tablaSimbolos[i]);
			//si hay elementos encadenados hay que liberarlos uno por uno, conservando el siguiente en una variable temporal, para no perderlo
			if(tablaSimbolos[i]->siguiente!=NULL){
				elementoTS *siguienteElemento=tablaSimbolos[i]->siguiente;
				while(siguienteElemento!=NULL){									//comprueba si está libre
					elementoTS *elementoTemporal=siguienteElemento->siguiente;	//guarda el siguiente en la variable temporal
					liberarElemento(siguienteElemento);
					free(siguienteElemento);
					siguienteElemento=elementoTemporal;							//vuelve a guardar el siguiente en la variable original
				}
			}
			//libera la posición del array TS
			free(tablaSimbolos[i]);
				tablaSimbolos[i]=NULL;
		}
	}

	//libera la TS
	if(tablaSimbolos!=NULL){
		free(tablaSimbolos);
			tablaSimbolos=NULL;
	}
}


//muestra la TS por pantalla
void imprimirTS(){
	printf("\n\nTabla de Símbolos\n");
	for(unsigned int i=0;i<tamanoTS;i++){
		if(tablaSimbolos[i]!=NULL){
			printf("\n[%d]",i);
			if(tablaSimbolos[i]->lexema!=NULL){
				printf("<");
				switch(tablaSimbolos[i]->tipo){
					case TIPO_FUN:
						printf("F %s (%s)> ",tablaSimbolos[i]->lexema,tablaSimbolos[i]->utilidad.fun->argumentos);
						break;
					case TIPO_CON:
						printf("C %s=%f> ",tablaSimbolos[i]->lexema,tablaSimbolos[i]->utilidad.valor);
						break;
					case TIPO_VAR:
						printf("V %s=%f> ",tablaSimbolos[i]->lexema,tablaSimbolos[i]->utilidad.valor);
						break;
					case TIPO_VAR_U:
						printf("VU %s=%f> ",tablaSimbolos[i]->lexema,tablaSimbolos[i]->utilidad.valor);
						break;
					default:
						//TODO error
						break;
				}
			}
			//recorre y muestra los elementos encadenados
			if(tablaSimbolos[i]->siguiente!=NULL){
				elementoTS *siguienteElemento=tablaSimbolos[i]->siguiente;
				while(siguienteElemento!=NULL){								//comprueba si está libre
					if(siguienteElemento->lexema!=NULL){
						printf(" \t-> ");
					}
					printf("<");
					switch(siguienteElemento->tipo){
						case TIPO_FUN:
							printf("F %s (%s)> ",siguienteElemento->lexema,siguienteElemento->utilidad.fun->argumentos);
							break;
						case TIPO_CON:
							printf("C %s=%f> ",siguienteElemento->lexema,siguienteElemento->utilidad.valor);
							break;
						case TIPO_VAR:
							printf("V %s=%f> ",siguienteElemento->lexema,siguienteElemento->utilidad.valor);
							break;
						case TIPO_VAR_U:
							printf("VU %s=%f> ",siguienteElemento->lexema,siguienteElemento->utilidad.valor);
							break;
						default:
							//TODO error
							break;
					}
					siguienteElemento=siguienteElemento->siguiente;			//se pasa al siguiente elemento
				}
			}
		}
	}
}

//reserva memoria para la TS e inicializa a NULL para marcar que no se usan los valores por ahora
void inicializarTS(){
	if(tablaSimbolos==NULL){
		if((tablaSimbolos=(elementoTS **)malloc(sizeof(elementoTS)*tamanoTS))==NULL){
			error(6, "la TS");
		}
		for(unsigned int i=0;i<tamanoTS;i++){
			tablaSimbolos[i]=NULL;
		}
	}else{
		error(7, "La TS");
	}
}

//busca si el elemento existe en la TS
//devuelve NULL si no existe
//devuelve elementoTS si existe
elementoTS* buscarElementoTS(char *lexema, char tipo){
	unsigned int posicion=hash(lexema,tamanoTS);											//calcula la posición
	if(tablaSimbolos[posicion]!=NULL && tablaSimbolos[posicion]->lexema!=NULL){
		if(strcmp(lexema, tablaSimbolos[posicion]->lexema)==0 && tablaSimbolos[posicion]->tipo==tipo){
			return tablaSimbolos[posicion];
		}else if(tablaSimbolos[posicion]->siguiente!=NULL){
			elementoTS* siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){										//comprueba si es alguno de los posibles encadenados
				if(strcmp(lexema,siguienteElemento->siguiente->lexema)==0 && siguienteElemento->siguiente->tipo==tipo){
					return siguienteElemento->siguiente;
				}
				siguienteElemento=siguienteElemento->siguiente;
			}
		}
	}
	return NULL;
}

//muestra información sobre todos los lexemas de la TS iguales, lo cual actualmente es solo 1
void imprimirLexemaTS(char *lexema){
	unsigned int posicion=hash(lexema,tamanoTS);											//calcula la posición
	if(tablaSimbolos[posicion]!=NULL && tablaSimbolos[posicion]->lexema!=NULL){
		if(strcmp(lexema, tablaSimbolos[posicion]->lexema)==0){
			switch(tablaSimbolos[posicion]->tipo){
				case TIPO_FUN:
					if(tablaSimbolos[posicion]->utilidad.fun!=NULL && tablaSimbolos[posicion]->utilidad.fun->argumentos!=NULL)
						printf("\tFunción %s (%s)\n",lexema, tablaSimbolos[posicion]->utilidad.fun->argumentos);
					break;
				case TIPO_CON:
					printf("\tConstante %s=%f\n",lexema, tablaSimbolos[posicion]->utilidad.valor);
					break;
				case TIPO_VAR:
					printf("\tVariable %s=%f\n",lexema, tablaSimbolos[posicion]->utilidad.valor);
					break;
				case TIPO_VAR_U:
					printf("\tVariable %s=%f\n",lexema, tablaSimbolos[posicion]->utilidad.valor);
					break;
				default:
					//TODO error
					break;
			}
		}
		if(tablaSimbolos[posicion]->siguiente!=NULL){
			elementoTS* siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){										//comprueba si es alguno de los posibles encadenados
				if(strcmp(lexema,siguienteElemento->siguiente->lexema)==0){
					switch(tablaSimbolos[posicion]->tipo){
						case TIPO_FUN:
							if(siguienteElemento->siguiente->utilidad.fun!=NULL && siguienteElemento->siguiente->utilidad.fun->argumentos!=NULL)
								printf("\tFunción %s (%s)\n",lexema, siguienteElemento->siguiente->utilidad.fun->argumentos);
							break;
						case TIPO_CON:
							printf("\tConstante %s=%f\n",lexema, siguienteElemento->siguiente->utilidad.valor);
							break;
						case TIPO_VAR:
							printf("\tVariable %s=%f\n",lexema, siguienteElemento->siguiente->utilidad.valor);
							break;
						case TIPO_VAR_U:
							printf("\tVariable %s=%f\n",lexema, siguienteElemento->siguiente->utilidad.valor);
							break;
						default:
							//TODO error
							break;
					}
				}
				siguienteElemento=siguienteElemento->siguiente;
			}
		}
	}
}

//muestra por pantalla cada elemento del tipo introducido, sirve para mostrar todas las variables, constantes o funciones
//se han dejado las no inicializadas pero realmente no se usan actualmente
void imprimirTipoTS(char tipo){
	for(unsigned int i=0;i<tamanoTS;i++){
		if(tablaSimbolos[i]!=NULL && tablaSimbolos[i]->lexema!=NULL){
			if(tablaSimbolos[i]->tipo==tipo){
				switch(tipo){
					case TIPO_FUN:
						if(tablaSimbolos[i]->utilidad.fun!=NULL && tablaSimbolos[i]->utilidad.fun->argumentos!=NULL)
							printf("\t%s (%s)\n",tablaSimbolos[i]->lexema, tablaSimbolos[i]->utilidad.fun->argumentos);
						break;
					case TIPO_CON:
						printf("\t%s=%f\n",tablaSimbolos[i]->lexema, tablaSimbolos[i]->utilidad.valor);
						break;
					case TIPO_VAR:
						printf("\t%s=%f\n",tablaSimbolos[i]->lexema, tablaSimbolos[i]->utilidad.valor);
						break;
					case TIPO_VAR_U:
						printf("\t%s=%f\n",tablaSimbolos[i]->lexema, tablaSimbolos[i]->utilidad.valor);
						break;
					default:
						//TODO error
						break;
				}
			}
			if(tablaSimbolos[i]->siguiente!=NULL){
				elementoTS* siguienteElemento=tablaSimbolos[i];
				while(siguienteElemento->siguiente!=NULL){										//comprueba si es alguno de los posibles encadenados
					if(siguienteElemento->siguiente->tipo==tipo){
						switch(tipo){
							case TIPO_FUN:
								if(siguienteElemento->siguiente->utilidad.fun!=NULL && siguienteElemento->siguiente->utilidad.fun->argumentos!=NULL)
									printf("\t%s (%s)\n",siguienteElemento->siguiente->lexema, siguienteElemento->siguiente->utilidad.fun->argumentos);
								break;
							case TIPO_CON:
								printf("\t%s=%f\n",siguienteElemento->siguiente->lexema, siguienteElemento->siguiente->utilidad.valor);
								break;
							case TIPO_VAR:
								printf("\t%s=%f\n",siguienteElemento->siguiente->lexema, siguienteElemento->siguiente->utilidad.valor);
								break;
							case TIPO_VAR_U:
								printf("\t%s=%f\n",siguienteElemento->siguiente->lexema, siguienteElemento->siguiente->utilidad.valor);
								break;
							default:
								//TODO error
								break;
						}
					}
					siguienteElemento=siguienteElemento->siguiente;
				}
			}
		}
	}
}

//muestra ayuda general
void ayuda(){
	printf("\n--------------------Ayuda--------------------\n");
	printf("Acceder a la ayuda del programa: '"ANSI_COLOR_YELLOW"help"ANSI_COLOR_RESET"', '"ANSI_COLOR_YELLOW"print"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"ayuda"ANSI_COLOR_RESET"'. Son equivalentes.\n");
	printf("Buscar ayuda particular (función, constante o variable): '"ANSI_COLOR_YELLOW"help"ANSI_COLOR_RESET"' por ejemplo '"ANSI_COLOR_YELLOW"help sin"ANSI_COLOR_RESET"' imprime ayuda de la función seno (si se ha cargado la librería Math).\n");

	printf("Las funciones siguen la sintaxis de C, tanto para declararse como para ejecutarse.\n");
	printf("Cabe notar que todas las funciones trigonométricas (de libm) funcionan en radianes.\n");
	printf("Actualmente no se puede repetir nombre, aunque uno haga referencia a una variable y otro a función.\n");
	printf("Por defecto se cargan varias constantes básicas para operaciones matemáticas.\n");
	printf("Las expresiones se pueden ejecutar con o sin paréntesis, con cualquier cantidad si están balanceados y se pueden llamar a funciones dentro de otras. Por ejemplo '"ANSI_COLOR_YELLOW"max(3, 3+4, min((2*4), 21-(3**2)))"ANSI_COLOR_RESET"'.\n");
	printf("Los cálculos terminados en '"ANSI_COLOR_YELLOW";"ANSI_COLOR_RESET"' no ofrecen salida pero se siguen ejecutando.\n");
	printf("Se pueden usar comentarios estilo C '"ANSI_COLOR_YELLOW"//comentado"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"/* comentado */"ANSI_COLOR_RESET"' .\n");
	printf("Si la calculadora detecta correctamente de forma autómatica los argumentos de una función se muestran en su descripción\n");
	printf("True es cualquier número >=1 y false <1. Se pueden comprobar con las funciones: '"ANSI_COLOR_YELLOW"isTrue número"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"isFalse expresión"ANSI_COLOR_RESET"'.\n");

	printf("Operaciones numéricas: suma '"ANSI_COLOR_YELLOW"+"ANSI_COLOR_RESET"', resta '"ANSI_COLOR_YELLOW"-"ANSI_COLOR_RESET"', multiplicación '"ANSI_COLOR_YELLOW"*"ANSI_COLOR_RESET"', división '"ANSI_COLOR_YELLOW"/"ANSI_COLOR_RESET"', exponenciación '"ANSI_COLOR_YELLOW"**"ANSI_COLOR_RESET"', módulo '"ANSI_COLOR_YELLOW"%%"ANSI_COLOR_RESET"'.\n");
	printf("Operaciones lógicas: AND '"ANSI_COLOR_YELLOW"&&"ANSI_COLOR_RESET"', OR '"ANSI_COLOR_YELLOW"||"ANSI_COLOR_RESET"', NOT '"ANSI_COLOR_YELLOW"!"ANSI_COLOR_RESET"'.\n");
	printf("Operaciones de bits: AND '"ANSI_COLOR_YELLOW"&"ANSI_COLOR_RESET"', OR '"ANSI_COLOR_YELLOW"|"ANSI_COLOR_RESET"', XOR '"ANSI_COLOR_YELLOW"^"ANSI_COLOR_RESET"'.\n");
	printf("Funciones para encontrar máximo y mínimo: '"ANSI_COLOR_YELLOW"max(número , número , número)"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"min (número, expresión)"ANSI_COLOR_RESET"'.\n");
	printf("Hay una aproximación a función de derivada: '"ANSI_COLOR_YELLOW"derivada función número"ANSI_COLOR_RESET"', por ejemplo '"ANSI_COLOR_YELLOW"derivada sin 0"ANSI_COLOR_RESET"' es 1.\n");

	printf("Se pueden declarar y emplear variables '"ANSI_COLOR_YELLOW"a=3"ANSI_COLOR_RESET"', '"ANSI_COLOR_YELLOW"b=3.2424"ANSI_COLOR_RESET"', '"ANSI_COLOR_YELLOW"b=a"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"a=b=c=3.5"ANSI_COLOR_RESET"' y mostrar su valor tal que '"ANSI_COLOR_YELLOW"help a"ANSI_COLOR_RESET"'.\n");
	printf("Lo mismo para constantes, excepto que se declaran tal que '"ANSI_COLOR_YELLOW"A=:3"ANSI_COLOR_RESET"'.\n");

	printf("Mostrar librerías del sistema que contengan un string o todas: '"ANSI_COLOR_YELLOW"librerias \"libm\""ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"librerias"ANSI_COLOR_RESET"'.\n");
	printf("Mostrar todas las variables: '"ANSI_COLOR_YELLOW"variables"ANSI_COLOR_RESET"'.\n");
	printf("Mostrar todas las constantes: '"ANSI_COLOR_YELLOW"constantes"ANSI_COLOR_RESET"'.\n");
	printf("Mostrar todas las funciones: '"ANSI_COLOR_YELLOW"funciones"ANSI_COLOR_RESET"'.\n");
	printf("Mostrar tipo y valor (variable, constante o función): '"ANSI_COLOR_YELLOW"tipo \"nombre\""ANSI_COLOR_RESET"'.\n");
	printf("Mostrar ayuda de las funciones más relevantes de Math: '"ANSI_COLOR_YELLOW"ayudaMath"ANSI_COLOR_RESET"'.\n");
	printf("Mostrar bits: '"ANSI_COLOR_YELLOW"binario expresión"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"binario número"ANSI_COLOR_RESET"'.\n");
	printf("Cargar librería del sistema: '"ANSI_COLOR_YELLOW"cL \"libm\""ANSI_COLOR_RESET"'. Otra libería matemática mucho más completa es gsl.\n");
	printf("Cargar archivo de librería: '"ANSI_COLOR_YELLOW"cA \"/lib/x86_64-linux-gnu/libm.so.6\""ANSI_COLOR_RESET"' (este depende del sistema).\n");
	printf("Cargar librería Math: '"ANSI_COLOR_YELLOW"cM"ANSI_COLOR_RESET"', '"ANSI_COLOR_YELLOW"cL \"libm\""ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"cA \"/lib/x86_64-linux-gnu/libm.so.6\""ANSI_COLOR_RESET"'.\n");
	printf("Cargar script (en lenguaje de la calculadora): '"ANSI_COLOR_YELLOW"cS \"fuente.m\""ANSI_COLOR_RESET"'.\n");
	printf("Salir del programa: '"ANSI_COLOR_YELLOW"salir"ANSI_COLOR_RESET"', '"ANSI_COLOR_YELLOW"exit"ANSI_COLOR_RESET"' o '"ANSI_COLOR_YELLOW"quit"ANSI_COLOR_RESET"'.\n");
	printf("\n"); 														//queda mejor con un salto de linea final
}

//ayuda de algunas de las funciones más importantes de Math.h o libm
void ayudaMath(){
	printf("double acos(double x): \tReturns the arc cosine of x in radians\n");
	printf("double asin(double x): \tReturns the arc sine of x in radians\n");
	printf("double atan(double x): \tReturns the arc tangent of x in radians\n");
	printf("double atan2(double y, double x): \tReturns the arc tangent in radians of y/x based on the signs of both values to determine the correct quadrant\n");
	printf("double cos(double x): \tReturns the cosine of a radian angle x\n");
	printf("double cosh(double x): \tReturns the hyperbolic cosine of x\n");
	printf("double sin(double x): \tReturns the sine of a radian angle x\n");
	printf("double sinh(double x): \tReturns the hyperbolic sine of x\n");
	printf("double tanh(double x): \tReturns the hyperbolic tangent of x\n");
	printf("double exp(double x): \tReturns the value of e raised to the xth power\n");
	printf("double frexp(double x, int *exponent): \tThe returned value is the mantissa and the integer pointed to by exponent is the exponent. The resultant value is x = mantissa * 2 ** exponent\n");
	printf("double ldexp(double x, int exponent): \tReturns x multiplied by 2 raised to the power of exponent\n");
	printf("double ln(double x): \tReturns the natural logarithm (base-e logarithm) of x\n");
	printf("double log(double x): \tReturns the natural logarithm (base-e logarithm) of x\n");
	printf("double log10(double x): \tReturns the common logarithm (base-10 logarithm) of x\n");
	printf("double modf(double x, double *integer): \tThe returned value is the fraction component (part after the decimal), and sets integer to the integer component\n");
	printf("double pow(double x, double y): \tReturns x raised to the power of y\n");
	printf("double sqrt(double x): \tReturns the square root of x\n");
	printf("double ceil(double x): \tReturns the smallest integer value greater than or equal to x\n");
	printf("double fabs(double x): \tReturns the absolute value of x\n");
	printf("double floor(double x): \tReturns the largest integer value less than or equal to x\n");
	printf("double fmod(double x, double y): \tReturns the remainder of x divided by y\n");
	printf("\n");
}


//muestra ayuda de función nombre si existe
void ayudaFUN(char *nombre){
	elementoTS* elemento=buscarElementoTS(nombre,TIPO_FUN);
	if(elemento!=NULL){
		printf("\tFunción %s (%s)\n",nombre, elemento->utilidad.fun->argumentos);
	}else{
		printf("\tNo se ha encontrado ninguna variable ni función %s\n",nombre);
	}
}


//busca en la TS si esa posicion no tiene ese lexema o si es una variable no inicializada que se ha colado
//devuelve NULL si no existe o es no inicializada
//devuelve el elementoTS en caso contrario
elementoTS* buscarLibre(char *lexema){
	unsigned int posicion=hash(lexema,tamanoTS);											//calcula la posición
	if(tablaSimbolos[posicion]!=NULL && tablaSimbolos[posicion]->lexema!=NULL){
		char tipo=tablaSimbolos[posicion]->tipo; 											//para reducir texto
		if(strcmp(lexema, tablaSimbolos[posicion]->lexema)==0){
			if(tipo==TIPO_VAR_U){ 															//variable sin definir: se libera porque fue un error
				free(tablaSimbolos[posicion]->lexema);
					tablaSimbolos[posicion]->lexema=NULL;
				free(tablaSimbolos[posicion]);
					tablaSimbolos[posicion]=NULL;
				return NULL;
			}else if(tipo==TIPO_VAR||tipo==TIPO_CON){
				printf("\t%s ya existe = %f\n",lexema,tablaSimbolos[posicion]->utilidad.valor);
				return tablaSimbolos[posicion];
			}else{
				printf("\t%s ya existe (%s)\n",lexema,tablaSimbolos[posicion]->utilidad.fun->argumentos);
				return tablaSimbolos[posicion];
			}
		}else if(tablaSimbolos[posicion]->siguiente!=NULL){
			elementoTS* siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){										//comprueba si es alguno de los posibles encadenados
				if(strcmp(lexema,siguienteElemento->siguiente->lexema)==0){
					tipo=siguienteElemento->siguiente->tipo;
					if(tipo==TIPO_VAR_U){ 													//variable sin definir: se libera porque fue un error
						free(siguienteElemento->siguiente->lexema);
							siguienteElemento->siguiente->lexema=NULL;
						free(siguienteElemento->siguiente);
							siguienteElemento->siguiente=NULL;
						return NULL;
					}else if(tipo==TIPO_VAR || tipo==TIPO_CON){
						printf("\t%s ya existe = %f\n",lexema,siguienteElemento->siguiente->utilidad.valor);
						return siguienteElemento->siguiente;
					}else{
						printf("\t%s ya existe (%s)\n",lexema,siguienteElemento->siguiente->utilidad.fun->argumentos);
						return siguienteElemento->siguiente;
					}
				}
				siguienteElemento=siguienteElemento->siguiente;
			}
		}
	}
	return NULL;
}

//muestra variables y constantes, además de borrar las variables no inicializadas que encuentra
void ayudaVAR(char *lexema){
	unsigned int posicion=hash(lexema,tamanoTS);											//calcula la posición
	if(tablaSimbolos[posicion]!=NULL && tablaSimbolos[posicion]->lexema!=NULL){
		char tipo=tablaSimbolos[posicion]->tipo; 											//para reducir texto
		if(strcmp(lexema, tablaSimbolos[posicion]->lexema)==0){
			if(tipo==TIPO_VAR_U){ 															//variable sin definir: se libera porque fue un error
				printf("\t%s no existe\n",lexema);
				free(tablaSimbolos[posicion]->lexema);
					tablaSimbolos[posicion]->lexema=NULL;
				free(tablaSimbolos[posicion]);
					tablaSimbolos[posicion]=NULL;
			}else if(tipo==TIPO_VAR||tipo==TIPO_CON){ 										//definida: se imprime
				printf("\t%s=%f\n",lexema,tablaSimbolos[posicion]->utilidad.valor);
			}
		}else if(tablaSimbolos[posicion]->siguiente!=NULL){
			elementoTS* siguienteElemento=tablaSimbolos[posicion];
			while(siguienteElemento->siguiente!=NULL){										//comprueba si es alguno de los posibles encadenados
				if(strcmp(lexema,siguienteElemento->siguiente->lexema)==0){
					tipo=siguienteElemento->siguiente->tipo;
					if(tipo==TIPO_VAR_U){ 													//variable sin definir: se libera porque fue un error
						printf("\t%s no existe\n",lexema);
						free(siguienteElemento->siguiente->lexema);
							siguienteElemento->siguiente->lexema=NULL;
						free(siguienteElemento->siguiente);
							siguienteElemento->siguiente=NULL;
						return;
					}else if(tipo==TIPO_VAR || tipo==TIPO_CON){								//definida: se imprime
						printf("\t%s=%f\n",lexema,siguienteElemento->siguiente->utilidad.valor);
					}
				}
				siguienteElemento=siguienteElemento->siguiente;
			}
		}
	}
}

//carga constantes en la TS
void cargarConstantes(){
	insertarTS("PI", 3.1415926535, TIPO_CON, NULL, "");
	insertarTS("E", 2.7182818284, TIPO_CON, NULL, "");
	insertarTS("Pytágoras", 1.41421, TIPO_CON, NULL, "");
	insertarTS("Theodorus", 1.73205080756887729352744634150587236 , TIPO_CON, NULL, "");
	insertarTS("Feigenbaum_alpha", 2.5029, TIPO_CON, NULL, "");
	insertarTS("Feigenbaum_delta", 4.6692, TIPO_CON, NULL, "");
	insertarTS("Apéry", 1.2020569, TIPO_CON, NULL, "");
	insertarTS("Número_áureo", 1.6180339887498948482, TIPO_CON, NULL, "");
	insertarTS("Euler_Mascheroni_y", 0.57721, TIPO_CON, NULL, "");
	insertarTS("Conway", 1.30357, TIPO_CON, NULL, "");
	insertarTS("Khinchin", 2.6854520010, TIPO_CON, NULL, "");
	insertarTS("Glaisher_Kinkelin", 1.2824271291, TIPO_CON, NULL, "");
	insertarTS("Meissel_Mertens", 0.26149721284764278375542683860869585, TIPO_CON, NULL, "");
	insertarTS("Bernstein", 0.28016949902386913303, TIPO_CON, NULL, "");
	insertarTS("Gauss_Kuzmin_Wirsing", 0.30366300289873265859744812190155623, TIPO_CON, NULL, "");
	insertarTS("Landau", 0.5, TIPO_CON, NULL, "");
	insertarTS("Omega", 0.56714329040978387299996866221035554, TIPO_CON, NULL, "");
	insertarTS("Laplace", 0.66274341934918158097474209710925290, TIPO_CON, NULL, "");
}

//prepara la TS para su uso por analizadorLexico()
void cargarTS(){
	inicializarTS();
	cargarConstantes();
}
