#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "error.h"
#include "tablaSimbolos.h"
#include "pilaArgumentos.h"
#include "librerias.h"
#include "flex.h"


//libera memoria y cierra el archivo
void liberar(){
	/*imprimirTS(); 																//para debugear el proceso*/
	liberarTS(); 																//libera la Tabla de Símbolos
	liberarPA(); 																//libera la Pila de Argumentos
	liberarHandle(); 															//libera la lista de handles
	yylex_destroy(); 															//libera la estructura de flex
}

//simplifica el código
void salir(unsigned int e){														//recibe código error
	fprintf(stderr, ANSI_COLOR_RED "\nFinalizando el programa\n" ANSI_COLOR_RESET );
	liberar();
	exit(e);
}

//comentadas las secuencias disponibles pero no usadas
void error(unsigned int e, char* sError){										//recibe código error y string opcional para más información
	//se usa yytext para ahorrarse copiar memoria
	switch(e){
		case 0:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tAl abrir el fichero %s\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 1:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tAl cerrar el fichero %s\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 2:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError reservando memoria para string %s\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 4:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError malloc para elemento de la TS con lexema: %s\n",sError);
			salir(e);
		case 5:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError malloc para string de la TS con lexema: %s\n",sError);
			salir(e);
		case 6:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError malloc para %s\n",sError);
			salir(e);
		case 7:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\t%s ya existe\n",sError);
			salir(e);
		case 8:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError reservando memoria para string %s en carga de librería\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 9:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tAl leer el fichero %s\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);

		case 10:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tRecibido valor incorrecto %s en el análisis léxico\n" ANSI_COLOR_RESET , yytext);
			break;

		case 20:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tNo existe ningún archivo del sistema para la libería %s\n" ANSI_COLOR_RESET, sError);
			break;
		case 21:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tNo se ha pasado ningún archivo a la función %s\n" ANSI_COLOR_RESET, sError);
			break;
		case 22:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tNo existe el archivo %s\n" ANSI_COLOR_RESET, sError);
			break;
		case 23:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tError interno en la función %s\n" ANSI_COLOR_RESET, sError);
			break;

		case 30:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tNo se ha reconocido la secuencia %s\n" ANSI_COLOR_RESET, sError);
			break;
		case 31:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_MAGENTA "\tNo se puede dividir por 0\n" ANSI_COLOR_RESET);
			break;

		case 40:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError malloc para elemento de la TA\n");
			salir(e);
		case 41:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tError realloc para %s\n",sError);
			salir(e);


		case 50:
			fprintf(stderr, ANSI_COLOR_GREEN "Error %d", e );
			fprintf(stderr, ANSI_COLOR_GREEN "\tUna runa octal debe tener 3 dígitos octales: %s\n" ANSI_COLOR_RESET , yytext);
			break;
		case 51:
			fprintf(stderr, ANSI_COLOR_YELLOW "Error %d", e );
			fprintf(stderr, ANSI_COLOR_YELLOW "\tEscapado un carácter no permitido: %s\n" ANSI_COLOR_RESET , yytext);
			break;
		case 52:
			fprintf(stderr, ANSI_COLOR_YELLOW "Error %d", e );
			fprintf(stderr, ANSI_COLOR_YELLOW "\tDemasiados caracteres para la runa: %s\n" ANSI_COLOR_RESET , yytext);
			break;
		case 53:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tNo existe la secuencia: ..\n" ANSI_COLOR_RESET );
			break;
		case 54:
			fprintf(stderr, ANSI_COLOR_GREEN "Error %d", e );
			fprintf(stderr, ANSI_COLOR_GREEN "\tUna runa hexadecimal debe tener 2 letras hexadecimales: %s\n" ANSI_COLOR_RESET , yytext);
			break;
		case 55:
			fprintf(stderr, ANSI_COLOR_GREEN "Error %d", e );
			fprintf(stderr, ANSI_COLOR_GREEN "\tUna runa Little U debe tener 4 letras hexadecimales: %s\n" ANSI_COLOR_RESET , yytext);
			break;
		case 56:
			fprintf(stderr, ANSI_COLOR_GREEN "Error %d", e );
			fprintf(stderr, ANSI_COLOR_GREEN "\tUna runa Big U debe tener 8 letras hexadecimales: %s\n" ANSI_COLOR_RESET , yytext);
			break;

		case 60:
			fprintf(stderr, ANSI_COLOR_RED "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tRecibidos 0 argumentos en la función %s\n",sError);
			salir(e);

		case 70:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tAl abrir el fichero %s\nError FATAL\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 71:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tNo se pudo cargar la librería de uso interno para cargar funciones del usuario %s\n", sError);
			perror("Información adicional del sistema: ");
			salir(e);
		case 72:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tDemasiados argumentos\n" ANSI_COLOR_RESET );
			break;
		case 73:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tNo existe el fichero %s\n" ANSI_COLOR_RESET , sError);
			break;
		case 74:
			fprintf(stderr, ANSI_COLOR_MAGENTA "Error %d", e );
			fprintf(stderr, ANSI_COLOR_RED "\tNo se puede sobrepasar el límite de archivos abiertos del sistema\n" ANSI_COLOR_RESET);
			break;

		default:
			fprintf(stderr, ANSI_COLOR_CYAN "Error %d", e );
			fprintf(stderr, ANSI_COLOR_CYAN "\tGravedad leve\n" ANSI_COLOR_RESET );
			break;
	}
}
