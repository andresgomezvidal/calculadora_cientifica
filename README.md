# calculadora_cientifica
Calculadora creada para práctica de Compiladores



Compilar y ejecutar el programa con el comando:

	make all && ./main
Se pueden eliminar los archivos de compilación con el comando:

	make clean
Se puede comprobar que se libera memoria correctamente con el comando (es necesario instalar valgrind):

	make all && valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes -v ./main



El programa muestra la siguiente ayuda al iniciarse:

--------------------Ayuda--------------------

Acceder a la ayuda del programa: 'help', 'print' o 'ayuda'. Son equivalentes.

Buscar ayuda particular (función, constante o variable): 'help' por ejemplo 'help sin' imprime ayuda de la función seno (si se ha cargado la librería Math).

Las funciones siguen la sintaxis de C, tanto para declararse como para ejecutarse.

Cabe notar que todas las funciones trigonométricas (de libm) funcionan en radianes.

Actualmente no se puede repetir nombre, aunque uno haga referencia a una variable y otro a función.

Por defecto se cargan varias constantes básicas para operaciones matemáticas.

Las expresiones se pueden ejecutar con o sin paréntesis, con cualquier cantidad si están balanceados y se pueden llamar a funciones dentro de otras.

	Por ejemplo 'max(3, 3+4, min((2*4), 21-(3**2)))'.


Los cálculos terminados en ';' no ofrecen salida pero se siguen ejecutando.

Se pueden usar comentarios estilo C '//comentado' o '/* comentado */' .

Si la calculadora detecta correctamente de forma autómatica los argumentos de una función se muestran en su descripción

True es cualquier número >=1 y false <1. Se pueden comprobar con las funciones: 'isTrue número' o 'isFalse expresión'.

Operaciones numéricas: suma '+', resta '-', multiplicación '*', división '/', exponenciación '**', módulo '%'.

Operaciones lógicas: AND '&&', OR '||', NOT '!'.

Operaciones de bits: AND '&', OR '|', XOR '^'.

Funciones para encontrar máximo y mínimo: 'max(número , número , número)' o 'min (número, expresión)'.

Hay una aproximación a función de derivada: 'derivada función número', por ejemplo 'derivada sin 0' es 1.

Se pueden declarar y emplear variables 'a=3', 'b=3.2424', 'b=a' o 'a=b=c=3.5' y mostrar su valor tal que 'help a'.

Lo mismo para constantes, excepto que se declaran tal que 'A=:3'.

Mostrar librerías del sistema que contengan un string o todas: 'librerias "libm"' o 'librerias'.

Mostrar todas las variables: 'variables'.

Mostrar todas las constantes: 'constantes'.

Mostrar todas las funciones: 'funciones'.

Mostrar tipo y valor (variable, constante o función): 'tipo "nombre"'.

Mostrar ayuda de las funciones más relevantes de Math: 'ayudaMath'.

Mostrar bits: 'binario expresión' o 'binario número'.

Cargar librería del sistema: 'cL "libm"'. Otra libería matemática mucho más completa es gsl.

Cargar archivo de librería: 'cA "/lib/x86_64-linux-gnu/libm.so.6"' (este depende del sistema).

Cargar librería Math: 'cM', 'cL "libm"' o 'cA "/lib/x86_64-linux-gnu/libm.so.6"'.

Cargar script (en lenguaje de la calculadora): 'cS "fuente.m"'.

Salir del programa: 'salir', 'exit' o 'quit'.




A continuación se explica un poco el planteamiento y decisiones de la práctica.

Explicaciones generales de la calculadora:

La carga de librerías del sistema se filtrando los argumentos cuando se puede (necesitan haber sido compiladas de forma particular). Son 
obtenidos con gdb. Si no se pueden obtener se sigue incluyendo la librería, pero no se introduce información sobre ellos. Esta información solo sirve de ayuda informativa sobre como debe ser llamada la función.

En clase se dijo que se podian usar comandos como grep y para hacer esta parte más cómoda uso varios scripts en vez de hacer llamadas desde C continuamente.

El proceso es básicamente obtener la lista de nombres de funciones del archivo .so con 'nm -CD', se guardan en un archivo y luego se examina el archivo .so para determinar si contiene información de debug, en cuyo caso se filtran los argumentos y se guardan en un archivo (con nombre en base al .so) en /tmp para poder cargarlo luego sin repetir el proceso, que puede llevar 2-5 segundos. Se decidió usar /tmp para que no dependiese del sistema y no estar metiendo algo molesto para el usuario por si luego quiere borrarlo, no tener que hacerlo directamente porque /tmp ya se borra automáticamente al arrancar el sistema.


También se usa 'ldconfig -p' para buscar las librerías del sistema en la caché /etc/ld.so.cache, que son las que se pueden usar típicamente.

Como los nombres de las constantes en Math.h son un poco raros y con mi método se añadirián valores que no le interesan al usuario creo que es mejor añadir las constantes elementales a mano.
La combinación con el hecho de poder procesar librerías sin haber sido compiladas con información de debug permite ejecutar el programa correctamente en varias distribuciones (he probado Ubuntu 16.04, Ubuntu de la escuela y Arch).

Hice un intento de generar wrappers para cualquier cantidad de argumentos de forma dinámica pero no funciona (son los archivos .bak). Probablemente se deba a un problema de contexto de memoria, donde el nuevo archivo no tiene acceso a los punteros en runtime, por lo que el puntero que recibe no tiene sentido. En cualquier caso no creo que tenga relevancia porque hice a mano con una macro hasta 50 argumentos.

También intenté poner el límite de archivos abiertos con 'ulimit -n' pero curiosamente devuelve 1036 en vez de 1024 como desde el shell directamente, por lo que he puesto el valor típico manualmente. Creo que podría haber usado funciones y structuras propias de C pero no estoy seguro. Tampoco creo que importe casi.

La relación bison/flex y el paso de tokens no es perfecto. Hago varios apaños con yytext e inserciones y borrados en la TS, pero creo que aun así cumple su objetivo principal, que es proveer un método para procesar cualquier tipo de entrada de la forma deseada.

He dedicado una parte del esfuerzo a usar correctamente estructuras dinámicas para gestionar las diferentes necesidades del programa, por ejemplo:

Lista de ficheros en formato de la calculadora abiertos (porque un fichero puede abrir otros). He comprobado que si se hace recursivo no se alcanza ulimit porque he dejado un pequeño margen, de forma que el programa no se cierra y se libera memoria correctamente.

Lista de handlers, para los archivos de funciones que define el usuario, para poder cerrarlos al terminar y que se libere memoria correctamente.

Pila de argumentos, para poder guardar cualquier cantidad de argumentos para cualquier cantidad de funciones. Es una pila con estructuras linqueadas. Se usa en los wrappers para enviar los argumentos necesarios.


La idea del wrapper el pasarle un puntero y que compruebe la cantidad de argumentos de la pila y ejecute la función dependiendo de eso con los que hagan falta. Para eso hay que escribir el código porque C no tiene estos mecanismos de abstracción, como por ejemplo eval en shell. Al terminar se hace pop de la pila.

El push se hace cuando se detecta una nueva secuencia de argumentos, que con mi grámatica ocurre al llegar al último elemento de la regla y a su vez se añaden los argumentos a la lista enlazada al terminar el procesado de cada sustitución. Para las funciones sin argumentos hace falta añadir una nueva opción, porque necesitan del push (para balancear pop y push) pero no de introducir argumentos (en vez de push también se puede usar una variable que indique si hacer pop o no, o ponerle a la regla args un tipo devuelto).

args:

		/*nada: funciones sin argumentos*/ 	{ addPA(); }
		| expresion							{ addPA(); insertarPA($1); }
		| args ',' expresion				{ insertarPA($3); }
	;

expresion:

	...
		| FUN_T '(' args ')' 				{ $$ = wrapper(*($1->utilidad.fun->fnct)); subPA(); }
		| MAX_T '(' args ')'				{ $$ = max(); subPA();}
		| MIN_T '(' args ')'				{ $$ = min(); subPA();}
	...


	double wrapper(double (*ptr)()){
		unsigned int nargs=nArgs();
		switch(nargs){
			case 1:
				return ((*ptr)(getArg(0)));
				break;
			case 2:
				return ((*ptr)(getArg(0),getArg(1)));
				break;
			case 3:
				return ((*ptr)(getArg(0),getArg(1),getArg(2)));
				break;
	...



La calculadora emplea doubles a priori.

La liberación se hace siempre en caso de error, por eso se encuentra en error.c.

Se adjuntan scripts de ejemplo para la carga de instrucciones con sintaxis de la calculadora.
