#ifndef OPERACIONES_H									//entra si no se ha cargado el header previamente
#define OPERACIONES_H									//marca que se ha cargado el header

#define TRUE 1
#define FALSE 2


void binario(double x);
void or(double d1, double d2);
void xor(double d1, double d2);
void and(double d1, double d2);

double derivar(double (*f)(double), double x0);
double isTrue(double v);

double max();
double min();

#endif
