#!/usr/bin/env bash

#$1 archivo a comprobar si tiene información de debug

#devuelve 0 si tiene información de debug
#devuelve 1 si no tiene información de debug
#devuelve 2 si ninguna de las anteriores, por ejemplo si no es ejecutable

sym=`gdb <<< "file $1"` 								#guarda en una variable el resultado de cargar esa librería

#comprueba si la salida contenía el string (no debugging symbols found), que significa que no se pueden obtener argumentos de las funciones y por tanto no interesa usarlo
if [[ `echo -n "${sym#*"..."}"` == *"Reading symbols from"* ]]; then
	echo 0 												#tiene información de debug
	exit 0
elif [[ `echo -n "${sym#*"..."}"` == *"(no debugging symbols found)"* ]]; then
	echo 1 												#no tiene información de debug
	exit 1
else
	echo 2 												#cualquier otra cosa, como que no sea ejecutable
	exit 2
fi
