%{
#include "error.h" 															//funciones de error y liberación
#include "tablaSimbolos.h" 													//operaciones de la TS
#include "string.h" 														//para tener los TOKEN
#include "calc.tab.h" 														//para tener los TOKEN

#include <stdio.h>
#include <stdlib.h>


void inicializarArchivos();
void cargarScript(char* path);
int recibido_EOF();
int getUlimit();

int contador_archivos=-1;
int ulimit=1024;
FILE** archivos;
%}

/* eliminación de warnings por yyunput e input */
%option nounput
%option noinput
/* elimina la necesidad de linkear al compilar */
%option noyywrap


/* identificadores para simplificar las expresiones regulares (ER) */
DIGITO            [0-9]
DECIMAL           (0|[1-9]{DIGITO}*)
EXP               [eE][\+-]?[0-9]+
FLOTANTE          ({DIGITO}*[.]{DIGITO}*{EXP}?|{DIGITO}+{EXP}|[.]{DIGITO}+{EXP}?)
IMAGINARIO        ({DECIMAL}|{FLOTANTE})i

/* no se han tenido en cuenta los caracteres unicode */
LETRA             [a-zA-Z_]
ID                {LETRA}({LETRA}|{DIGITO})*

STR               \".*\"

TIPO              "double"|"float"|"int"|"long"|"short"
NFUNCION          {TIPO}[ \t]+{ID}

%%

{DECIMAL}              								{yylval.val = atof(yytext); return NUM_T;}
{FLOTANTE} 			   								{yylval.val = atof(yytext); return NUM_T;}

{STR} 												yylval.string=quitar_comillas(yytext); return STR_T;

"derivada" 											return DER_T;

"funciones" 										return MOSTRAR_F_T;
"variables" 										return MOSTRAR_V_T;
"constantes" 										return MOSTRAR_C_T;
"librerias"											return MOSTRAR_L_T;
"tipo"												return MOSTRAR_T_T;

"max" 												return MAX_T;
"min" 												return MIN_T;

"ayudaMath" 										return AYUDA_MATH_T;
"print"|"help"|"ayuda" 								return AYUDA_T;

"salir"|"exit"|"quit" 								return recibido_EOF();

"binario" 											return BIN_T;

"cS"[ ]{STR}										cargarScript(quitar_comillas(extrae_palabra_2(yytext))); return '\n';

"cM" 												return CARGAR_M_T;
"cL" 												return CARGAR_L_T;
"cA" 												return CARGAR_A_T;

"isTrue" 											return TRUE_T;
"isFalse" 											return '!';

"\n"												return '\n';
"=" 												return '=';
"+"													return '+';
"-"													return '-';
"*"													return '*';
"/"													return '/';
"^"													return '^';
"**"												return EXP_T;
"%"													return '%';
"("													return '(';
")"													return ')';
","													return ',';
";"													return ';';
":"													return ':';
"&"													return '&';
"|"													return '|';
"!"													return '!';
"&&"												return AND_T;
"||"												return OR_T;


{NFUNCION} 											yylval.string=extrae_palabra_2(yytext); return NFUN_T;

[ \t]+         										//espacios y tabs

"//".*    											//comentario simple
[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/] 				//comentario multi-línea


 /* se inserta en la TS y devuelve el elementoTS */
{ID} 					yylval.elem=insertarTS(yytext, 0, TIPO_VAR_U, 0, ""); if(yylval.elem->tipo==TIPO_FUN) return FUN_T; else return VAR_T;




 /* se cumple solo si no se han cumplido las reglas anteriores, por tanto solo si no se detecta nada conocido */
.           										error(10, "" );



<<EOF>> 											return recibido_EOF();

%%


/*también podría usar getrlimit*/
int getUlimit(){
	size_t longitud,tamano;
	char* resultado=NULL;
	int valor=102;
	FILE* tuberia = popen("ulimit -n", "r");
	if(tuberia==NULL){
		error(0, "tubería en getUlimit()");
	}

	//solo debe devolver una línea
	if((longitud=getline(&resultado, &tamano, tuberia)) != -1) { 			//lee líneas hasta terminar la salida del comando
		resultado[longitud-1] = '\0'; 										//elimina el salto de línea
		valor=atoi(resultado);
	}
	printf("\tulimit=%s=%d\n",resultado,valor);
	free(resultado);

	if(tuberia!=NULL && fclose(tuberia)!=0){
		error(1, "tubería en getUlimit()");
	}

	return valor;
}

//para resolver el problema de recursión se usa un array de streams que puede aumentarse dinámicamente
void inicializarArchivos(){
	if(archivos==NULL){
		/*ulimit=getUlimit(); 												//devuelve 1036 en vez de 1024*/
		if((archivos=(FILE **)malloc(sizeof(FILE*)*ulimit))==NULL){
			error(6, "array de archivos");
		}
		for(unsigned int i=0;i<ulimit;i++){
			archivos[i]=NULL;
		}
	}
}

//devuelve token de salida si se ha cerrado el archivo actual o lo cierra. Es necesario porque hay que comprobar el estado para hacer uno u otro
int recibido_EOF(){
	if(archivos!=NULL && contador_archivos>=0 && archivos[contador_archivos]!=NULL){
		if(fclose(archivos[contador_archivos])!=0){
			error(1, "elemento del array de archivos en recibido_EOF()");
		}
		contador_archivos--;

		yypop_buffer_state();
		return '\n';
	}else{
		if(archivos!=NULL){
			free(archivos);
			archivos=NULL;
		}
		return SALIR_T;
	}
}

void cargarScript(char* path){
	inicializarArchivos(); 													//evita tener que incluir .h desde bison, para no crear un bucle
	contador_archivos++;
	if(contador_archivos+10>=ulimit){ 										//el margen permite realizar otras operaciones de archivos en el peor caso
		error(74, "");
		contador_archivos--;
		return;
	}

	archivos[contador_archivos]=fopen(path,"r");
	if(archivos[contador_archivos]==NULL){
		error(73, path);
		contador_archivos--;
		return;
	}

	printf("\tCargando script %s ...\n",path);
	yyin=archivos[contador_archivos];
	yypush_buffer_state(yy_create_buffer( yyin, YY_BUF_SIZE ));
}
