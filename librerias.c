#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>

#include "error.h"
#include "librerias.h"
#include "tablaSimbolos.h"
#include "pilaArgumentos.h"

//archivos de uso temporal para comandos o para almacenar información de interés para nuevas ejecuciones futuras
#define LIB_TMP         "/tmp/calculadora_archivo_temporal"
#define LIB_TMP2        "/tmp/calculadora_archivo_temporal2"
#define STR_TMP         "/tmp/calculadora_"
#define BUSCAR_SH       "./buscarLibreria.sh"
#define DEBUG_SH        "./comprobarDebug.sh"
#define SFUN_TMP        "/tmp/calculadora_funcion_"

unsigned int contador_handle=-1;
unsigned int tamano_handle=100;
void** handle_usuario;

//hace la reserva inicial de la lista de punteros
void inicializarHandle(){
	if(handle_usuario==NULL){
		if((handle_usuario=(void **)malloc(sizeof(void*)*tamano_handle))==NULL){
			error(6, "array de handles");
		}
		for(unsigned int i=0;i<tamano_handle;i++){
			handle_usuario[i]=NULL;
		}
	}else{
		error(7, "Array de handles");
	}
}

//comprueba si un archivo existe
//devuelve 1 si existe
//devuelve 0 si NO existe
int existeArchivo(char *ruta){
	FILE *archivo=fopen(ruta,"r");
	if(archivo==NULL){													//no existe
		return 0; 														//no hace falta cerrarlo porque no se ha abierto
	}else{
		fclose(archivo);
		return 1;
	}
}

//lista librerías con ldconfig -p y se filtran por la entrada recibida si existe
//equivalente a ejecutar el comando en el shell, pero integrado en la calculadora porque puede que el usuario no lo sepa o no se acuerde
void mostrarLibreria(char *lib){
	char *resultado=NULL;
	char *comando=NULL;
	char *ldconfig_1="ldconfig -p |awk '$1~/'";
	char *ldconfig_2="'.so/ {print $0}'";
	size_t tamano=0;
	size_t longitud=0;

	if(strcmp(lib, "")==0){													//no se pasó ninguna librería a buscar
		printf("Listando todas las librerías del sistema\n");
	}else{
		printf("Buscando librerías del sistema relacionada con %s\n",lib);
	}


	//construye el string para ejecutar el comando con el archivo de entrada
	int lenComando=strlen(ldconfig_1)+strlen(ldconfig_2)+strlen(lib);
	if((comando=(char *)malloc(1+sizeof(char)*lenComando))==NULL){
		error(8, "comando en mostrarLibreria()");
	}
	strcpy(comando,ldconfig_1);
	strcat(comando,lib);
	strcat(comando,ldconfig_2);

	FILE* tuberia = popen(comando, "r");
	if(tuberia==NULL){
		error(0, "tubería en mostrarLibreria()");
	}
	free(comando);

	while((longitud=getline(&resultado, &tamano, tuberia)) != -1) { 	//lee líneas hasta terminar la salida del comando
		printf("%s",resultado);
	}

	free(resultado);
	if(tuberia!=NULL && fclose(tuberia)!=0){
		error(1, "tubería en mostrarLibreria()");
	}
}

//busca la librería del sistema indicada y si existe la carga con o sin argumentos (dependiendo de si tiene información de debug)
void buscarLibreria(char *lib){
	if(strcmp(lib, "")==0){													//no se pasó ninguna librería a buscar
		error(21,"buscarLibreria()");
		return;
	}
	char *resultado=NULL;
	char *comando=NULL;
	size_t tamano=0;
	size_t longitud=0;

	printf("Buscando librería %s\n",lib);

	//construye el string para ejecutar el comando con el archivo de entrada
	int lenComando=strlen(BUSCAR_SH)+strlen(lib);
	if((comando=(char *)malloc(2+sizeof(char)*lenComando))==NULL){ 			//hay que dejar un espacio
		error(8, "comando en buscarLibreria()");
	}
	strcpy(comando,BUSCAR_SH);
	strcat(comando," ");
	strcat(comando,lib);

	//devuelve la libreria math (libm) del sistema si existe
	FILE* tuberia = popen(comando, "r");
	if(tuberia==NULL){
		error(0, "tubería en buscarLibreria()");
	}
	free(comando);

	//solo debe devolver una línea
	if((longitud=getline(&resultado, &tamano, tuberia)) == -1) { 		//lee línea de ruta archivo, 0 o 1
		error(9, "tubería en buscarLibreria()");
	}
	resultado[longitud-1] = '\0'; 										//elimina el salto de línea


	if(((strcmp(resultado, "1"))==0) || ((strcmp(resultado, ""))==0)){	//no se encontró nada, no debería ser nunca "" pero se mira
		error(20,lib);
		return;
	}else if(strcmp(resultado, "0")==0){								//no se encontró ninguna librería con información de argumentos
		if((longitud=getline(&resultado, &tamano, tuberia)) == -1) { 	//lee línea de ruta archivo
			error(9, "tubería en buscarLibreria()");
		}
		resultado[longitud-1] = '\0'; 									//elimina el salto de línea
		cargarDefiniciones(resultado, ARGS_NO);
	}else{
		cargarDefiniciones(resultado, ARGS_SI);
	}

	free(resultado);
	if(tuberia!=NULL && fclose(tuberia)!=0){
		error(1, "tubería en buscarLibreria()");
	}
}

//busca el archivo indicado y si existe y es ejecutable lo carga con o sin argumentos (dependiendo de si tiene información de debug)
void debugArchivo(char *arc){
	if(strcmp(arc, "")==0){													//no se pasó ningun archivo a buscar
		error(21,"debugArchivo()");
		return;
	}

	if(existeArchivo(arc)!=1){ 												//si no existe el archivo
		error(22,arc);
		return;
	}

	char *resultado=NULL;
	char *comando=NULL;
	size_t tamano=0;
	size_t longitud=0;

	//construye el string para ejecutar el comando con el archivo de entrada
	int lenComando=strlen(DEBUG_SH)+strlen(arc);
	if((comando=(char *)malloc(2+sizeof(char)*lenComando))==NULL){ 			//hay que dejar un espacio
		error(8, "comando en debugArchivo()");
	}
	strcpy(comando,DEBUG_SH);
	strcat(comando," ");
	strcat(comando,arc);

	//indica si la librería tiene informacion de debug
	FILE* tuberia = popen(comando, "r");
	if(tuberia==NULL){
		error(0, "tubería en debugArchivo()");
	}
	free(comando);

	//solo debe devolver una línea
	if((longitud=getline(&resultado, &tamano, tuberia)) == -1) { 		//lee línea de ruta archivo, 0 o 1
		error(9, "tubería en debugArchivo()");
	}
	resultado[longitud-1] = '\0'; 										//elimina el salto de línea


	if((strcmp(resultado, ""))==0){										//no se encontró nada, no debería ser nunca "" pero se mira
		error(23,"debugArchivo()");
	}else if(strcmp(resultado, "0")==0){								//no se encontró ninguna librería con información de argumentos
		cargarDefiniciones(arc, ARGS_SI);
	}else if(strcmp(resultado, "1")==0){								//no se encontró ninguna librería con información de argumentos
		cargarDefiniciones(arc, ARGS_NO);
	}else{
		error(23,"debugArchivo()");
	}

	free(resultado);
	if(tuberia!=NULL && fclose(tuberia)!=0){
		error(1, "tubería en debugArchivo()");
	}
}

//carga en la TS las definiciones del archivo indicado
//el archivo tiene un formato con línea con nombre de función seguida de línea con sus argumentos y así hasta terminar
void cargarDefiniciones(char *archivoLib, char cFunc){
	size_t tamano = 0;
	size_t longitud = 0;
	char *func = NULL;
	char *args = NULL;
	char *comando = NULL;
	char *archivoArgs; 												//guarda los argumentos procesados para una libreria particular

	//construye el string unico del nombre del archivo
	int lenArchivoArgs=strlen(STR_TMP)+strlen(archivoLib);
	if((archivoArgs =(char *)malloc(1+sizeof(char)*lenArchivoArgs))==NULL){
		error(8, "archivoArgs");
	}
	strcpy(archivoArgs,STR_TMP);
	for(int i=0;i<strlen(archivoLib);i++){
		if(archivoLib[i]=='/')
			archivoArgs[i+strlen(STR_TMP)]='_';
		else
			archivoArgs[i+strlen(STR_TMP)]=archivoLib[i];
	}
	archivoArgs[lenArchivoArgs]='\0';

	FILE * archivoFunciones=NULL;
	if(cFunc==ARGS_SI){
		if(existeArchivo(archivoArgs)==1){ 												//si existe el archivo
			printf("Empleando archivo procesado previamente: %s\n", archivoArgs);
			//abre el archivo, solo lectura
			archivoFunciones=fopen(archivoArgs, "rb");
			if(archivoFunciones==NULL){													//si hay un error al abrir el archivo
				error(0, archivoArgs);
			}
		}else{
			//filtra las funciones de la libreria y las escribe en el archivo indicado
			printf("Filtrando información de librería del sistema: %s\n", archivoLib);
			tamano = snprintf(NULL, 0, "nm -CD %s |awk '$3 ~/^[a-zA-Z]/ { print $3 }' > %s",archivoLib, LIB_TMP);
			if((comando =(char *)malloc(1+sizeof(char)*tamano))==NULL){
				error(8, "comando");
			}
			snprintf(comando, tamano+1, "nm -CD %s |awk '$3 ~/^[a-zA-Z]/ { print $3 }' > %s",archivoLib, LIB_TMP);
			system(comando);
			free(comando);

			//filtra los argumentos de las funciones, que obtiene de gdb
			tamano = snprintf(NULL, 0, "./filtra_argumentos.sh %s %s %s %s",LIB_TMP,archivoArgs,LIB_TMP2,archivoLib);
			if((comando =(char *)malloc(1+sizeof(char)*tamano))==NULL){
				error(8, "comando");
			}
			snprintf(comando, tamano+1, "./filtra_argumentos.sh %s %s %s %s",LIB_TMP,archivoArgs,LIB_TMP2,archivoLib);
			system(comando);
			free(comando);

			archivoFunciones=fopen(archivoArgs, "ab+");
			if(archivoFunciones==NULL){													//si hay un error al abrir el archivo
				error(0, archivoArgs);
			}
		}

		//añade funciones a la TS, con sus argumentos
		// Introduce already loaded functions to runtime linker's space
		void* handle = dlopen(archivoLib,RTLD_NOW|RTLD_GLOBAL);

		unsigned int fcont=0;
		while ((longitud = getline(&func, &tamano, archivoFunciones)) != -1) { 	//lee línea de nombre función
			func[longitud-1] = '\0'; 											//elimina el salto de línea
			if((longitud = getline(&args, &tamano, archivoFunciones)) != -1){ 	//lee línea de argumentos de función
				args[longitud-1] = '\0';										//elimina el salto de línea
				insertarTS(func, 0, TIPO_FUN, (double (*)(double))dlsym(handle, func), args);
				printf("Función %s (%s)\n",func,args);
				fcont++;
			}
		}
		printf("Añadidas %d funciones de la librería del sistema: %s\n", fcont, archivoLib);
	}else{ 																			//no se cargan argumentos
		//filtra las funciones de la libreria y las escribe en el archivo indicado
		printf("Filtrando información de librería del sistema: %s\n", archivoLib);
		tamano = snprintf(NULL, 0, "nm -CD %s |awk '$3 ~/^[a-zA-Z]/ { print $3 }' > %s",archivoLib, LIB_TMP);
		if((comando =(char *)malloc(1+sizeof(char)*tamano))==NULL){
			error(8, "comando");
		}
		snprintf(comando, tamano+1, "nm -CD %s |awk '$3 ~/^[a-zA-Z]/ { print $3 }' > %s",archivoLib, LIB_TMP);
		system(comando);
		free(comando);

		//abre el archivo para solo lectura
		archivoFunciones=fopen(LIB_TMP, "rb");
		if(archivoFunciones==NULL){													//si hay un error al abrir el archivo
			error(0, LIB_TMP);
		}
		// Introduce already loaded functions to runtime linker's space
		void* handle = dlopen(archivoLib,RTLD_NOW|RTLD_GLOBAL);

		unsigned int fcont=0;
		while ((longitud = getline(&func, &tamano, archivoFunciones)) != -1) { 	//lee línea de nombre función
			func[longitud-1] = '\0'; 											//elimina el salto de línea
			insertarTS(func, 0, TIPO_FUN, (double (*)(double))dlsym(handle, func), "");
			printf("Función %s (%s)\n",func,"");
			fcont++;
		}
		printf("Añadidas %d funciones de la librería del sistema: %s\n\n", fcont, archivoLib);
	}

	if(func!=NULL)
		free(func);
	if(args!=NULL)
		free(args);
	if(archivoArgs!=NULL)
		free(archivoArgs);
	if(archivoFunciones!=NULL && fclose(archivoFunciones)!=0){
		error(1, archivoArgs);
	}
}

//crea función del usuario, si el lexema no está en uso
//para ello crea un archivo (.c) con la función, lo compila como librería (.so) y lo linquea dinámicamente
//es importante no cerrar el handle hasta el final del programa, porque hace falta para liberar la memoria correctamente pero se pierden las funciones
//por eso se usa una lista de handles, dinámica porque no se sabe el tamaño que puede necesitar
void crearFuncion(char* nombre, char *fun_str){
	if(buscarLibre(nombre)!=NULL){ 				//verifica que el lexema no está en uso (si es variable no inicializada la borra y usa esa posición)
		return;
	}

	//crea strings archivo c y so
	contador_handle++;
	if(contador_handle>=tamano_handle){
		if((handle_usuario=(void **)realloc(handle_usuario,sizeof(void*)*(contador_handle*2)))==NULL){
			error(41, "handle_usuario");
		}
	}

	char *archivo_so=NULL;
	char *archivo_c=NULL;
	size_t tamano;

	tamano = snprintf(NULL, 0, "%s%d.so",SFUN_TMP, contador_handle);
	if((archivo_so =(char *)malloc(1+sizeof(char)*tamano))==NULL){
		error(8, "archivo_so");
	}
	snprintf(archivo_so, tamano+1, "%s%d.so",SFUN_TMP, contador_handle);

	tamano = snprintf(NULL, 0, "%s%d.c",SFUN_TMP, contador_handle);
	if((archivo_c =(char *)malloc(1+sizeof(char)*tamano))==NULL){
		error(8, "archivo_c");
	}
	snprintf(archivo_c, tamano+1, "%s%d.c",SFUN_TMP, contador_handle);

	//abre archivo
	FILE *f = fopen(archivo_c, "w");
	if(f == NULL){
		error(70,archivo_c);
	}
	//escribir
	fprintf(f, "double %s %s",nombre,fun_str);
	fclose(f);

	//compilar
	char *comando;
	tamano = snprintf(NULL, 0, "gcc -shared -o %s -fPIC %s",archivo_so, archivo_c);
	if((comando =(char *)malloc(1+sizeof(char)*tamano))==NULL){
		error(8, "comando");
	}
	snprintf(comando, tamano+1, "gcc -shared -o %s -fPIC %s",archivo_so, archivo_c);
	system(comando);
	free(comando);

	//linkear y añadir la función a la TS
	//guarda el handle en el array
	handle_usuario[contador_handle] = dlopen(archivo_so,RTLD_NOW|RTLD_GLOBAL);
	if(handle_usuario[contador_handle]==NULL){
		error(71,archivo_so);
	}

	//filtra los argumentos: para introducirlos al insertar en la TS
	//mucho más eficiente que el otro método, pero obviamente no está disponible para esos casos
	int i;
	char balance=-1;
	char p=0;
	char l=strlen(fun_str)-1;
	for(i=0; i<strlen(fun_str); i++){ 									//encuentra principio y fin paréntesis iniciales
		if(fun_str[i]=='('){
			if(balance==-1){
				p=i+1;
				balance=1;
			}else
				balance++;
		}else if(fun_str[i]==')'){
			balance--;
			if(balance==0)
				l=i-1;
		}
	}
	for(i=0; i<l-p+1; i++){
		fun_str[i]=fun_str[i+p];
	}
	fun_str[i]='\0';

	insertarTS(nombre, 0, TIPO_FUN, (double (*)(double))dlsym(handle_usuario[contador_handle], nombre), fun_str);
	printf("\tFunción añadida: double %s (%s)\n",nombre,fun_str);

	free(archivo_c);
	free(archivo_so);
}

//libera todos los handles
void liberarHandle(){
	for(unsigned int i=0;i<contador_handle+1;i++){
		if(dlclose(handle_usuario[i])!=0){
			error(71,"");
		}
	}
	free(handle_usuario);
}


//permite llamar a la función (pasada como puntero) con la cantidad de argumentos necesarios, determinados por nArgs()
double wrapper(double (*ptr)()){
	unsigned int nargs=nArgs();
	switch(nargs){
		case 1:
			return ((*ptr)(getArg(0)));
			break;
		case 2:
			return ((*ptr)(getArg(0),getArg(1)));
			break;
		case 3:
			return ((*ptr)(getArg(0),getArg(1),getArg(2)));
			break;
		case 4:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3));
			break;
		case 5:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4));
			break;
		case 6:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5));
			break;
		case 7:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6));
			break;
		case 8:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7));
			break;
		case 9:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8));
			break;
		case 10:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9));
			break;
		case 11:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10));
			break;
		case 12:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11));
			break;
		case 13:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12));
			break;
		case 14:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13));
			break;
		case 15:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14));
			break;
		case 16:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15));
			break;
		case 17:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16));
			break;
		case 18:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17));
			break;
		case 19:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18));
			break;
		case 20:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19));
			break;
		case 21:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20));
			break;
		case 22:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21));
			break;
		case 23:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22));
			break;
		case 24:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23));
			break;
		case 25:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24));
			break;
		case 26:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25));
			break;
		case 27:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26));
			break;
		case 28:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27));
			break;
		case 29:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28));
			break;
		case 30:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29));
			break;
		case 31:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30));
			break;
		case 32:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31));
			break;
		case 33:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32));
			break;
		case 34:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33));
			break;
		case 35:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34));
			break;
		case 36:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35));
			break;
		case 37:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36));
			break;
		case 38:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37));
			break;
		case 39:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38));
			break;
		case 40:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39));
			break;
		case 41:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40));
			break;
		case 42:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41));
			break;
		case 43:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42));
			break;
		case 44:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43));
			break;
		case 45:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44));
			break;
		case 46:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44),getArg(45));
			break;
		case 47:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44),getArg(45),getArg(46));
			break;
		case 48:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44),getArg(45),getArg(46),getArg(47));
			break;
		case 49:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44),getArg(45),getArg(46),getArg(47),getArg(48));
			break;
		case 50:
			return (*ptr)(getArg(0),getArg(1),getArg(2),getArg(3),getArg(4),getArg(5),getArg(6),getArg(7),getArg(8),getArg(9),getArg(10),getArg(11),getArg(12),getArg(13),getArg(14),getArg(15),getArg(16),getArg(17),getArg(18),getArg(19),getArg(20),getArg(21),getArg(22),getArg(23),getArg(24),getArg(25),getArg(26),getArg(27),getArg(28),getArg(29),getArg(30),getArg(31),getArg(32),getArg(33),getArg(34),getArg(35),getArg(36),getArg(37),getArg(38),getArg(39),getArg(40),getArg(41),getArg(42),getArg(43),getArg(44),getArg(45),getArg(46),getArg(47),getArg(48),getArg(49));
			break;
		default:
			error(72,"");
			return 0;
	}
}
