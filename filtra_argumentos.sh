#!/usr/bin/env bash



FIN="$1" 											#archivo de entrada con nombres de funciones
FOUT="$2" 											#archivo de salida con nombres de funciones y sus argumentos
FTEMP="$3" 											#archivo temporal
LIB="$4" 											#path archivo libreria del sistema


#se elimina la versión previa si existe, no deberia hacer falta por el overwrite on en gdb pero por si acaso
rm -f "$FTEMP"



while read -r i; do
funciones="$funciones
info functions ^__$i$"
done < "$FIN" 										#lee del archivo linea por linea, for tomaba el archivo entero


#se podria hacer attach de gdb al pid del proceso en ejecucion pero por defecto no se puede por la configuracion del kernel
gdb X <<<"
file $LIB
set pagination off
set logging redirect off
set logging file $FTEMP
set logging overwrite on
set logging off
set logging on
$funciones
set logging off
quit"  &>/dev/null


#gdb X <<<"
#file ./carga/main
#set pagination off
#set logging redirect off
#set logging file $FTEMP
#set logging overwrite on
#set logging off
#run $LIB
#set logging on
#$funciones
#set logging off
#quit"  &>/dev/null


while read -r i; do
	#guarda en la variable el contenido de dentro de los parentesis (argumentos de la funcion)
	#args="$(awk -F '_' '$NF ~/^'$i'\(.*\);/ {print $NF}' < $FTEMP | cut -d '(' -f2  | cut -d ')' -f1)"
	args="$(awk -F '_' '$NF ~/^'$i'\([a-zA-Z][a-zA-Z0-9, ]*\);/ {print $NF}' < $FTEMP | cut -d '(' -f2  | cut -d ')' -f1)"

	#añade las funciones que encuentran argumentos con el formato funcion argumentos, cada uno en una linea para facilitarlo en C
	#elimina casos en los que la funcion tiene punteros
	#if [[ -n "$args" && "$args" != *"*"* ]]; then
	if [ -n "$args" ]; then
		echo "$i" >> "$FOUT"
		echo "$args" >> "$FOUT"
	fi
done < "$FIN"
