unsigned int hash(char *str, unsigned int max){					//djb2 apadtado http://www.cse.yorku.ca/~oz/hash.html
	unsigned int hash = 5381;
	int c;

	while ((c = *str++)){
		hash = ((hash << 5) + hash) + c;						/* hash * 33 + c */
	}

	return hash%max;
}
