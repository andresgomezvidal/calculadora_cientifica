#ifndef PILAARGUMENTOS_H									//entra si no se ha cargado el header previamente
#define PILAARGUMENTOS_H									//marca que se ha cargado el header


//lista encadenada porque no se sabe cuantos argumentos va a haber
typedef struct elementoPA {
	double valor;
	struct elementoPA *siguiente;
} elementoPA;



elementoPA* generarElementoPA(double valor);
elementoPA* insertarPA(double valor);
void addPA();
void subPA();
void liberarPA();
void cargarPA();
double getArg(unsigned int pos);
unsigned int nArgs();


#endif
