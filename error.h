#ifndef ERROR_H									//entra si no se ha cargado el header previamente
#define ERROR_H									//marca que se ha cargado el header

//macros para diferenciar errores por el color
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void liberar();
void salir(unsigned int e);
void error(unsigned int e, char* sError);

#endif
