#include <stdlib.h>
#include <stdio.h>

#include "pilaArgumentos.h"
#include "error.h"


//funciona a modo de pila, o sea FIFO
//hace falta una pila, que puede crecer pero es poco probable por su tamaño inicial
elementoPA **pilaArgumentos=NULL;										//PA: pila de argumentos encadenados
unsigned int tope=-1; 													//posicion actual en la pila
char ARG_0=0; 															//variable para indicar que se ha liberado el argumento 0 para nuevos usos


//función auxiliar para simplificar el código
//devuelve el elemento a insertar en la PA
elementoPA* generarElementoPA(double valor){
	elementoPA *elemento=NULL;
	//reserva memoria para el elemento de la pila
	if((elemento=(elementoPA *)malloc(sizeof(elementoPA)))==NULL){										//si es NULL hubo un error
		error(40, "");
	}
	elemento->valor=valor;							//se guarda el valor
	elemento->siguiente=NULL;						//se marca con NULL para saber que no está en uso

	return elemento;
}

//inserta en la posición siguiente de la PA
elementoPA* insertarPA(double valor){
	if(pilaArgumentos[tope]==NULL || ARG_0==0){										//posición libre: se inserta directamente
		ARG_0=1;
		pilaArgumentos[tope]=generarElementoPA(valor);
		return pilaArgumentos[tope];
	}else{ 																			//posicion ocupada: se inserta en siguiente libre
		if(pilaArgumentos[tope]->siguiente!=NULL){									//busca hasta el primero libre
			elementoPA *siguienteElemento=pilaArgumentos[tope];
			while(siguienteElemento->siguiente!=NULL){								//comprueba si está libre
				siguienteElemento=siguienteElemento->siguiente;						//se pasa al siguiente elemento
			}
			siguienteElemento->siguiente=generarElementoPA(valor);					//inserta en último libre
			return siguienteElemento->siguiente;
		}else{																		//el siguiente esPAba libre: inserta
			pilaArgumentos[tope]->siguiente=generarElementoPA(valor);
			return pilaArgumentos[tope]->siguiente;
		}
	}
}


//comienza una nueva secuencia de argumentos
//cambia el valor tope y si hace falta reserva memoria
//es un push
void addPA(){
	tope++;
	if((pilaArgumentos=(elementoPA **)realloc(pilaArgumentos,sizeof(elementoPA)*(tope+1)))==NULL){
		error(41, "la TA");
	}
	pilaArgumentos[tope]=NULL;
}

//termina secuencia actual de argumentos
//cambia el valor tope y libera y pone a NULL la secuencia de argumentos encadenados
//es un pop
void subPA(){
	if(tope==0)
		ARG_0=0;
	if(pilaArgumentos[tope]!=NULL){
		//si hay elementos encadenados hay que liberarlos uno por uno, conservando el siguiente en una variable temporal, para no perderlo
		if(pilaArgumentos[tope]->siguiente!=NULL){
			elementoPA *siguienteElemento=pilaArgumentos[tope]->siguiente;
			while(siguienteElemento!=NULL){									//comprueba si está libre
				elementoPA *elementoTemporal=siguienteElemento->siguiente;	//guarda el siguiente en la variable temporal
				free(siguienteElemento);
				siguienteElemento=elementoTemporal;							//vuelve a guardar el siguiente en la variable original
			}
		}
			pilaArgumentos[tope]->siguiente=NULL;
			free(pilaArgumentos[tope]);
				pilaArgumentos[tope]=NULL;
	}
	tope--;
}

//libera de forma ordenada la PA
//siempre se compara con NULL para determinar si un puntero es usado
//adicionalmente (no hace falta en este caso) se ponen a NULL los párametros liberados
void liberarPA(){
	for(unsigned int i=0;i<(tope+1);i++){
		if(pilaArgumentos[i]!=NULL){
			//si hay elementos encadenados hay que liberarlos uno por uno, conservando el siguiente en una variable temporal, para no perderlo
			if(pilaArgumentos[i]->siguiente!=NULL){
				elementoPA *siguienteElemento=pilaArgumentos[i]->siguiente;
				while(siguienteElemento!=NULL){									//comprueba si está libre
					elementoPA *elementoTemporal=siguienteElemento->siguiente;	//guarda el siguiente en la variable temporal
					free(siguienteElemento);
					siguienteElemento=elementoTemporal;							//vuelve a guardar el siguiente en la variable original
				}
			}
			//libera la posición del array PA
			free(pilaArgumentos[i]);
				pilaArgumentos[i]=NULL;
		}
	}

	//libera la PA
	if(pilaArgumentos!=NULL){
		free(pilaArgumentos);
			pilaArgumentos=NULL;
	}
}

//devuelve el argumento de la posición indicada del tope de la pila
double getArg(unsigned int pos){
	unsigned int cantidad=0;
	if(pilaArgumentos[tope]!=NULL){
		if(cantidad==pos){
			return pilaArgumentos[tope]->valor;
		}
		//recorre los elementos encadenados y aumenta la variable
		if(pilaArgumentos[tope]->siguiente!=NULL){
			elementoPA *siguienteElemento=pilaArgumentos[tope]->siguiente;
			while(siguienteElemento!=NULL){								//comprueba si está libre
				cantidad++;
				if(cantidad==pos){
					return siguienteElemento->valor;
				}
				siguienteElemento=siguienteElemento->siguiente;			//se pasa al siguiente elemento
			}
		}
	}
	return 0;
}

//devuelve la cantidad de argumentos del tope de la pila
unsigned int nArgs(){
	unsigned int cantidad=1;
	if(pilaArgumentos[tope]!=NULL){
		//recorre los elementos encadenados y aumenta la variable
		if(pilaArgumentos[tope]->siguiente!=NULL){
			elementoPA *siguienteElemento=pilaArgumentos[tope]->siguiente;
			while(siguienteElemento!=NULL){								//comprueba si está libre
				cantidad++;
				siguienteElemento=siguienteElemento->siguiente;			//se pasa al siguiente elemento
			}
		}
	}
	return cantidad;
}
