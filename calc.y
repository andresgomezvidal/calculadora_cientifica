%{
#include "tablaSimbolos.h" 											//operaciones de la TS
#include "librerias.h" 												//carga de librerias y wrappers
#include "error.h" 													//funciones de error y liberación
#include "operaciones.h" 											//funciones para mostrar binario y operaciones a nivel de bit
#include "pilaArgumentos.h" 										//funciones para registrar varios argumentos

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

extern int yylex();
extern int yyparse();
extern FILE* yyin;

void yyerror(char* s);
%}

%union {
	double val;
	char *string;
	elementoTS *elem;
}


%type<val> expresion
%type<elem> asignacion

%token<elem> FUN_T VAR_T
%token<val> NUM_T
%token<string> STR_T NFUN_T
%token SALIR_T AYUDA_T AYUDA_MATH_T
%token CARGAR_A_T CARGAR_L_T CARGAR_M_T
%token AND_T OR_T BIN_T MAX_T MIN_T DER_T
%token TRUE_T
%token MOSTRAR_F_T MOSTRAR_V_T MOSTRAR_C_T MOSTRAR_L_T MOSTRAR_T_T
%token CARGAR_S_T

%left '='
%left '|'
%left '^'
%left '&'
%left AND_T OR_T
%left '!' TRUE_T
%left '+' '-'
%left '*' '/'
%left '%'
%left RES_T
%right EXP_T
%left FUN_T MAX_T MIN_T

%start calcular

%%

calcular:
	| calcular linea
;

linea:
	'\n'
	| ';'

    | asignacion '\n' 					{ printf("\t%f\n", $1->utilidad.valor); }
    | asignacion ';' '\n'				{ }

    | SALIR_T 							{ printf("\nSaliendo de la calculadora\n"); liberar(); exit(0); }


    | expresion '\n' 					{ printf("\t%f\n", $1);}
    | expresion ';'  					{ }

	| BIN_T expresion '\n' 				{ binario($2); }
	| BIN_T expresion ';' 				{ }
	| expresion '&' expresion '\n' 		{ and($1, $3); }
	| expresion '&' expresion ';' 		{ }
	| expresion '|' expresion '\n' 		{ or($1 , $3); }
	| expresion '|' expresion ';' 		{ }
	| expresion '^' expresion '\n' 		{ xor($1 , $3); }
	| expresion '^' expresion ';' 		{ }

	| CARGAR_A_T STR_T '\n' 			{ debugArchivo($2); }
	| CARGAR_A_T STR_T ';' 				{ debugArchivo($2); }
	| CARGAR_L_T STR_T '\n' 			{ buscarLibreria($2); }
	| CARGAR_L_T STR_T ';' 				{ buscarLibreria($2); }
	| CARGAR_M_T '\n'  					{ buscarLibreria("libm"); }
	| CARGAR_M_T ';'  					{ buscarLibreria("libm"); }

	| NFUN_T STR_T '\n' 				{ crearFuncion($1,$2); }
	| NFUN_T STR_T ';' 					{ crearFuncion($1,$2); }

	| MOSTRAR_F_T '\n'  				{ imprimirTipoTS(TIPO_FUN); }
	| MOSTRAR_F_T ';'  					{ imprimirTipoTS(TIPO_FUN); }
	| MOSTRAR_V_T '\n'  				{ imprimirTipoTS(TIPO_VAR); }
	| MOSTRAR_V_T ';'  					{ imprimirTipoTS(TIPO_VAR); }
	| MOSTRAR_C_T '\n'  				{ imprimirTipoTS(TIPO_CON); }
	| MOSTRAR_C_T ';'  					{ imprimirTipoTS(TIPO_CON); }

	| MOSTRAR_L_T STR_T '\n' 			{ mostrarLibreria($2); }
	| MOSTRAR_L_T STR_T ';' 			{ mostrarLibreria($2); }
	| MOSTRAR_L_T '\n'  				{ mostrarLibreria(""); }
	| MOSTRAR_L_T ';'  					{ mostrarLibreria(""); }

	| MOSTRAR_T_T STR_T '\n' 			{ imprimirLexemaTS($2); }
	| MOSTRAR_T_T STR_T ';' 			{ imprimirLexemaTS($2); }

	| AYUDA_MATH_T '\n'  				{ ayudaMath(); }
	| AYUDA_MATH_T ';'  				{ ayudaMath(); }
    | AYUDA_T '\n'  					{ ayuda(); }
    | AYUDA_T ';'  						{ ayuda(); }
    | AYUDA_T FUN_T '\n'  				{ ayudaFUN($2->lexema); }
    | AYUDA_T FUN_T ';'  				{ ayudaFUN($2->lexema); }
	| AYUDA_T VAR_T '\n'  				{ ayudaVAR($2->lexema); }
	| AYUDA_T VAR_T ';'  				{ ayudaVAR($2->lexema); }
;

asignacion:
	 VAR_T '=' expresion 				{ guardarVariable($1->lexema, $3); }
	|VAR_T '=' asignacion				{ guardarVariable($1->lexema, $3->utilidad.valor); }
	|VAR_T '=' ':' expresion			{ guardarConstante($1->lexema, $4); }
	|VAR_T '=' ':' asignacion			{ guardarConstante($1->lexema, $4->utilidad.valor); }
;

args:
	/*nada: funciones sin argumentos*/ 	{ addPA(); }
	| expresion							{ addPA(); insertarPA($1); }
	| args ',' expresion				{ insertarPA($3); }
;

expresion:
	NUM_T								{ $$ = $1; }
	| VAR_T 							{ $$ = $1->utilidad.valor; }

	| expresion '%' expresion			{ $$ = fmod($1 , $3); }
	| expresion '-' expresion			{ $$ = $1 - $3; }
	| expresion '+' expresion			{ $$ = $1 + $3; }
	| expresion '*' expresion 			{ $$ = $1 * $3; }
	| expresion '/' expresion			{
											if($3!=0){
												$$ = $1 / (double) $3;
											}else{
												error(31,""); $$=$1;
											}
										}

	| expresion EXP_T expresion			{ $$ = pow($1 , $3); }

	/*permite ejecutar expresiones entre paréntesis, los argumentos son separados por comas*/
	| '(' expresion ')'					{ $$ = $2; }

	| expresion AND_T expresion			{ $$ = $1 && $3; }
	| expresion OR_T expresion			{ $$ = $1 || $3; }

	/*ejecuta funciones de la TS mediante un wrapper para abstraerse de los argumentos*/
	| FUN_T '(' args ')' 				{ $$ = wrapper(*($1->utilidad.fun->fnct)); subPA(); }

	/*no neceistan wrapper porque ya tienen la funcionalidad de argumentos integrada en su código*/
	| MAX_T '(' args ')'				{ $$ = max(); subPA();}
	| MIN_T '(' args ')'				{ $$ = min(); subPA();}

	/*es una función de juguete básicamente*/
	| DER_T FUN_T expresion 			{ $$=derivar($2->utilidad.fun->fnct,$3); }

	/*true y false*/
	| '!' expresion						{ if($2<1)  $$ = 1; else $$=0; }
	| TRUE_T expresion					{ $$ = isTrue($2); }

	/* permiten poner una cantidad ilimitada de estos signos antes de la expresión como +---3 */
	| '-' expresion %prec RES_T			{ $$ = -$2; }
	| '+' expresion %prec RES_T			{ $$ = $2; }
;


%%


void cargar(){
	cargarTS();
	inicializarHandle();
	ayuda();
}

int main(int argc, char **argv){
	cargar();

	yyin = stdin;
	do {
		yyparse();
	}while(!feof(yyin));

	liberar();

	return 0;
}

void yyerror(char* s) {
	error(30, s);
}
