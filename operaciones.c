#include <stdio.h>
#include <stdint.h>

#include "operaciones.h"
#include "pilaArgumentos.h"
#include "error.h"

//muestra la cadena binaria del número double
void binario(double x) {
    union {
        double x;
        char c[sizeof(double)];
    } u;

    u.x = x;
    for (unsigned ofs = 0; ofs < sizeof(double); ofs++) {
        for(int i = 7; i >= 0; i--) {
            printf(((1 << i) & u.c[ofs]) ? "1" : "0");
        }
        printf(" ");
    }
    printf("\n");
}

//muestra la cadena binaria del número double resultante de un or
void or(double d1, double d2) {
    union {
        double d;
        char c[sizeof(double)];
    } u1;

    u1.d = d1;
    union {
        double d;
        char c[sizeof(double)];
    } u2;

    u2.d = d2;
    for (unsigned ofs = 0; ofs < sizeof(double); ofs++) {
        for(int i = 7; i >= 0; i--) {
            printf(((1 << i) & (u1.c[ofs] | u2.c[ofs])) ? "1" : "0");
        }
        printf(" ");
    }
    printf("\n");
}

//muestra la cadena binaria del número double resultante de un xor
void xor(double d1, double d2) {
    union {
        double d;
        char c[sizeof(double)];
    } u1;

    u1.d = d1;
    union {
        double d;
        char c[sizeof(double)];
    } u2;

    u2.d = d2;
    for (unsigned ofs = 0; ofs < sizeof(double); ofs++) {
        for(int i = 7; i >= 0; i--) {
            printf(((1 << i) & (u1.c[ofs] ^ u2.c[ofs])) ? "1" : "0");
        }
        printf(" ");
    }
    printf("\n");
}

//muestra la cadena binaria del número double resultante de un and
void and(double d1, double d2) {
    union {
        double d;
        char c[sizeof(double)];
    } u1;

    u1.d = d1;
    union {
        double d;
        char c[sizeof(double)];
    } u2;

    u2.d = d2;
    for (unsigned ofs = 0; ofs < sizeof(double); ofs++) {
        for(int i = 7; i >= 0; i--) {
            printf(((1 << i) & u1.c[ofs] & u2.c[ofs]) ? "1" : "0");
        }
        printf(" ");
    }
    printf("\n");
}

double isTrue(double v){
	if(v>=1)
		return TRUE;
	else
		return FALSE;
}

double derivar(double (*f)(double), double x0)
{
    const double delta = 1.0e-6; // or similar
    double x1 = x0 - delta;
    double x2 = x0 + delta;
    double y1 = f(x1);
    double y2 = f(x2);
    return (y2 - y1) / (x2 - x1);
}

//recorre todos los argumentos de la Pila de Argumentos y los compara
double max(){
	unsigned int nargs=nArgs();
	if(nargs==0)
		error(60,"max()");

	double res=getArg(0);
	for(unsigned int i=1; i <= nArgs()-1; i++) {
		double last=getArg(i);
		if(last>res)
			res = last;
	}

	return res;
}

//recorre todos los argumentos de la Pila de Argumentos y los compara
double min(){
	unsigned int nargs=nArgs();
	if(nargs==0)
		error(60,"max()");

	double res=getArg(0);
	for(unsigned int i=1; i <= nArgs()-1; i++) {
		double last=getArg(i);
		if(res>last)
			res = last;
	}

	return res;
}
