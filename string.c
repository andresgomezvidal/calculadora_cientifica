#include <string.h>
#include <stdlib.h>

#include "error.h"

//pasa string entrecomillado a su contenido, por ejemplo "aa" pasa a ser aa
char* quitar_comillas(char* string){
	int i;
	for(i=0; i<strlen(string)-2; i++)
		string[i]=string[i+1];
	string[i]='\0';
	return string;
}

//pasa string estilo "double func" a func
char* extrae_palabra_2(char* string){
	int i;
	char p=-1;
	char f=strlen(string)-1;
	for(i=0; i<strlen(string); i++){ 									//encuentra principio y fin palabra 2
		if(p==-1){ 														//no se ha llegado al primer espacio o tab
			if(string[i]==' ' || string[i]=='\t'){
				p=0;
			}
		}else if(p==0){ 												//encontrado primer caracter palabra 2
			if(string[i]!=' ' && string[i]!='\t'){
				p=i;
			}
		}else{
			if(string[i]==' ' || string[i]=='\t'){ 						//encontrado fin palabra 2
				f=i-1;
			}
		}
	}
	for(i=0; i<f-p+1; i++){
		string[i]=string[i+p];
	}

	string[i]='\0';
	return string;
}
