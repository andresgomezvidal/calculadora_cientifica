#ifndef LIBRERIAS_H									//entra si no se ha cargado el header previamente
#define LIBRERIAS_H									//marca que se ha cargado el header

//valores para determinar si es una libreria del usuario o del sistema
#define TIPO_LU 0
#define TIPO_LS 1

//indican si se carga o no una libreria con informacion de funciones
#define ARGS_SI 0
#define ARGS_NO 1

#include "tablaSimbolos.h"

int existeArchivo(char *ruta);
void buscarLibreria(char *lib);
void mostrarLibreria(char *lib);
void debugArchivo(char *arc);
void cargarDefiniciones(char *archivoLib, char cFunc);
double wrapper(double (*ptr)());
void crearFuncion(char* nombre, char *fun_str);
void liberarHandle();
void inicializarHandle();

#endif
