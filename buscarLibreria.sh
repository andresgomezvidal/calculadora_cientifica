#!/usr/bin/env bash

#$1 es el nombre de la librería a buscar, por ejemplo libm

#devuelve la ruta del archivo si tiene información de debug
#devuelve 0\narchivo si no tiene ninguno información de debug
#devuelve 1 si no se encuentran librerías con ese string
#devuelve 2 si no se pasa entrada al script

if [ -z "$1" ]; then
	echo 2 													#no se ha especificado nada a buscar
	exit 2
fi

#se da por hecho que las librerías devueltas son ejecutables
librerias="$(ldconfig -p |awk '$1~/'$1'.so/ {print $NF}')" 	#guarda en una variable los archivos del sistema de la librería indicada

if [ -z "$librerias" ]; then
	echo 1 													#no se ha encontrado nada
	exit 1
fi

while read -r i; do
	sym=`gdb <<< "file $i"` 								#guarda en una variable el resultado de cargar esa librería

	#comprueba si la salida contenía el string (no debugging symbols found), que significa que no se pueden obtener argumentos de las funciones y por tanto no interesa usarlo
	if [[ `echo -n "${sym#*"..."}"` != *"(no debugging symbols found)"* ]]; then
		echo "$i"
		exit 0
	fi
	last="$i"
done <<< "$librerias" 												#lee de la variable línea por línea, for tomaba todo entero


#devuelve el último archivo, por devolver siempre uno, y que el hecho de que no exista información de los argumentos no pare la carga de la librería
echo 0 														#se devuelve 0 para indicar que no se ha encontrado ninguna para obtener args
echo "$last"
