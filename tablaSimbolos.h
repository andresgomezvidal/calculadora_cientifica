#ifndef TABLASIMBOLOS_H									//entra si no se ha cargado el header previamente
#define TABLASIMBOLOS_H									//marca que se ha cargado el header

#define tamanoTS 1000

//el tipo puede ser 0 o 1
#define TIPO_FUN   0
#define TIPO_CON   1 									//constante
#define TIPO_VAR   2
#define TIPO_VAR_U 3 									//indica variable todavia no inicializada
														//la alternativa era marcarlo en valor, pero eso no es fiable (se pueden usar todos los valores)


//estructura para la función con puntero y argumentos
typedef struct sfuncion {
	double (*fnct)();
	char *argumentos;
} sfuncion;

//struct de elementos que contiene la TS
typedef struct elementoTS {
	char *lexema;
	char tipo;
	union {
		double valor;
		sfuncion *fun;
	} utilidad;
	struct elementoTS *siguiente;
} elementoTS;


elementoTS* generarElementoTS(char *lexema, double valor, char tipo, double (*fnct)(), char * argumentos);
elementoTS* insertarTS(char *lexema, double valor, char tipo, double (*fnct)(), char * argumentos);
void guardarConstante(char *lexema, double valor);
void guardarVariable(char *lexema, double valor);
void liberarElemento(elementoTS* elemento);
void liberarTS();
void imprimirTS();
void inicializarTS();
elementoTS* buscarElementoTS(char *lexema, char tipo);
elementoTS* buscarLibre(char *lexema);
void imprimirLexemaTS(char *lexema);
void imprimirTipoTS(char tipo);
void ayuda();
void ayudaMath();
void ayudaFUN(char *nombre);
void ayudaVAR(char *lexema);
void cargarConstantes();
void cargarTS();

#endif
